package com.graytrails.aws.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class LambdaFunctionHandler implements RequestHandler<S3Event, String> {

    private AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();

    public LambdaFunctionHandler() {}

    LambdaFunctionHandler(AmazonS3 s3) {
        this.s3 = s3;
    }


    public String handleRequest(S3Event event, Context context) {
        context.getLogger().log("Received event: " + event);

        // Get the object from the event and show its content type
        String bucket = event.getRecords().get(0).getS3().getBucket().getName();
        String key = event.getRecords().get(0).getS3().getObject().getKey();
        context.getLogger().log("copying from one bucket to another");
        s3.copyObject(bucket,  System.getenv("SOURCE_FILE_NAME"), System.getenv("DESTINATION_BUCKET_NAME"), System.getenv("DESTINATION_FILE_NAME")) ;

        // context.getLogger().log("downloading existing file copying to another s3bucket ");
        return "copied" ;


    }




}