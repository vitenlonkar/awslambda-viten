package com.graytrails.aws.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

import java.io.Serializable;

@DynamoDBTable(tableName = "Movies")
public class Movies {

    private Integer year ;
    private String title ;
    private Information info ;

    @DynamoDBHashKey(attributeName = "year")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }


    @DynamoDBRangeKey(attributeName = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @DynamoDBAttribute(attributeName = "info")

    public Information getInfo() {
        return info;
    }

    public void setInfo(Information info) {
        this.info = info;
    }
}
