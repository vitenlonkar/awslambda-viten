package com.graytrails.aws.lambda;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graytrails.aws.model.Movies;

public class LambdaFunctionHandlerToProcessSQSMessages implements RequestHandler<SQSEvent, Void> {


    //Region awsTableRegion = Region.getRegion(Regions.EU_CENTRAL_1);

    AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    public LambdaFunctionHandlerToProcessSQSMessages() {}

    public LambdaFunctionHandlerToProcessSQSMessages(AmazonDynamoDB client) {
        this.client = client;
    }
    public Void handleRequest(SQSEvent input, Context context) {
        context.getLogger().log("Started Lambda for SQS messaging to copy data to dynamodb");
        DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(client);
       /* DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable("Movies");*/
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            for(SQSEvent.SQSMessage msg : input.getRecords()){
                context.getLogger().log("msg: "+ msg.getBody());
                Movies movies = objectMapper.readValue(msg.getBody(), Movies.class);
                dynamoDBMapper.save(movies);

                context.getLogger().log("updated tables");
                //Code to Update individual item
                /*int year = 2013;
                String title = "Rush";

                UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                        .withPrimaryKey(new PrimaryKey("year", year)).withUpdateExpression("remove info.actors[0]")
                        .withConditionExpression("size(info.actors) >= :num").withValueMap(new ValueMap().withNumber(":num", 3))
                        .withReturnValues(ReturnValue.UPDATED_NEW);
                UpdateItemOutcome outcome = table.updateItem(updateItemSpec);*/

            }
        } catch (Exception e) {
            try {
                throw e ;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return null;
    }
}
