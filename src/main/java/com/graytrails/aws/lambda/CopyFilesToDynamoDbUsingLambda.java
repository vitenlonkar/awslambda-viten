package com.graytrails.aws.lambda;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.sqs.AmazonSQS;
import com.opencsv.CSVReader;



import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;


public class CopyFilesToDynamoDbUsingLambda implements RequestHandler<S3Event, String> {
    AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();

    AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();



    public CopyFilesToDynamoDbUsingLambda(){}
    public CopyFilesToDynamoDbUsingLambda(AmazonS3 s3) {
        this.s3 = s3;
    }
    public CopyFilesToDynamoDbUsingLambda(AmazonDynamoDB client) {
        this.client = client;
    }



    public String handleRequest(S3Event input, Context context) {
        String returnResult = null ;
        context.getLogger().log("Started Lambda event to copy data in dynamoDB");

        Region awsTableRegion = Region.getRegion(Regions.EU_CENTRAL_1);
        String tableName = "InverterDetail_TBL";


        try {
            S3EventNotification.S3EventNotificationRecord record = input.getRecords().get(0);

            String srcBucket = record.getS3().getBucket().getName();
            // Object key may have spaces or unicode non-ASCII characters.
            String srcKey = record.getS3().getObject().getKey()
                    .replace('+', ' ');
            srcKey = URLDecoder.decode(srcKey, "UTF-8");


            S3Object s3Object = s3.getObject(new GetObjectRequest(srcBucket, srcKey));
            InputStreamReader isr = new InputStreamReader(s3Object.getObjectContent()) ;
            BufferedReader bfr = new BufferedReader(isr);
            CSVReader csvReader = new CSVReader(bfr) ;

            DynamoDB dynamoDB = new DynamoDB(client);

           // TableWriteItems energyDataTableWriteItems = new TableWriteItems(tableName);
            Table table = dynamoDB.getTable(tableName);

            //List<Item> itemList = new ArrayList<Item>();

            String[] nextLine;

            while ((nextLine = csvReader.readNext()) != null) {
                if(!(nextLine[0].equalsIgnoreCase("date"))) {

                    Item newItem = new Item();
                    table.putItem(newItem.withPrimaryKey("date", nextLine[0])
                            .withString("inverterMode", nextLine[1])
                            .withString("temperature", nextLine[2]));
                }

            }

            returnResult = "success";

        } catch (Exception e) {
           e.printStackTrace();
        }



    return returnResult ;

    }
}
