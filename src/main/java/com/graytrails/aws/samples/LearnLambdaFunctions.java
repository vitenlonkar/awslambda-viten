package com.graytrails.aws.samples;

import com.graytrails.aws.model.Movies;
import com.graytrails.aws.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class LearnLambdaFunctions  {

    static List<Person> personList = new ArrayList<>() ;
    static List<Movies> movieList = new ArrayList<>() ;
    //LearnLamdaFunctionInterface learnLamdaFunctionInterface = new LearnLambdaFunctions() ;

    static {
        Person person = new Person() ;
        person.setAge(26);
        person.setName("Viten");
        person.setLastName("Lonkar");
        person.setSex(Person.Sex.MALE);
        personList.add(person) ;

        Person person1 = new Person() ;
        person1.setAge(25);
        person1.setName("Akshay");
        person1.setLastName("Kumar");
        person1.setSex(Person.Sex.MALE);
        personList.add(person1) ;

        Person person2 = new Person() ;
        person2.setAge(18);
        person2.setName("Rita");
        person2.setLastName("Ray");
        person2.setSex(Person.Sex.FEMALE);
        personList.add(person2) ;

    }

    public static void main(String args[]) {

        //printPersons(personList, new LearnLambdaFunctions());
        printPersons(personList,
                (Person p) -> p.getSex() == Person.Sex.MALE
                && p.getAge() >= 18
        );
        printPersonsUsingPredicate(personList,
                (Person p) -> p.getLastName().equalsIgnoreCase("Lonkar"));


    }


    public boolean test(Person p) {
        return p.getSex() == Person.Sex.FEMALE
                && p.getAge() == 18;
    }

    public static void printPersons(
            List<Person> roster, LearnLamdaFunctionInterface tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
               System.out.println(p.toString());
            }
        }
    }


    public static void printPersonsUsingPredicate(
            List<Person> roster, Predicate<Person> predicate) {
        for (Person p : roster) {
            if (predicate.test(p)) {
                System.out.println(p.toString());
            }
        }
    }

    public static void printMoviesUsingPredicate(
            List<Movies> roster, Predicate<Movies> predicate) {
        for (Movies p : roster) {
            if (predicate.test(p)) {
                System.out.println(p.getYear());
            }
        }
    }


    interface Predicate<T> {
        boolean test(T p);
    }


}
