package com.graytrails.aws.lambdatest;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.graytrails.aws.lambda.LambdaToProcessFileAndPostMessage;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

public class LambdaToProcessFileAndPostMessageTest {

    private static S3Event input;

    @BeforeClass
    public static void createInput() throws IOException {

        input = TestUtils.parse("/s3-event.json", S3Event.class, null);
    }

    private Context createContext() {
        TestContext ctx = new TestContext();
        ctx.setFunctionName("handleRequest");
        return ctx;
    }

    @Test
    public void testLambdaFunctionHandler() {
        LambdaToProcessFileAndPostMessage handler = new LambdaToProcessFileAndPostMessage();
        Context ctx = createContext();
        String output = handler.handleRequest(input, ctx);
        if (output != null) {
            System.out.println(output.toString());
        }
    }
}



