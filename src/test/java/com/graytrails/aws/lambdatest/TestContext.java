package com.graytrails.aws.lambdatest;

import com.amazonaws.services.lambda.runtime.ClientContext;
import com.amazonaws.services.lambda.runtime.CognitoIdentity;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class TestContext implements Context {

    private String awsRequestId = "EXAMPLE";
    private ClientContext clientContext;
    private String functionName = "EXAMPLE";
    private CognitoIdentity identity;
    private String logGroupName = "EXAMPLE";
    private String logStreamName = "EXAMPLE";
    private LambdaLogger logger = new TestLogger();
    private int memoryLimitInMB = 128;
    private int remainingTimeInMillis = 15000;


    public void setAwsRequestId(String awsRequestId) {
        this.awsRequestId = awsRequestId;
    }

    public void setClientContext(ClientContext clientContext) {
        this.clientContext = clientContext;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public void setIdentity(CognitoIdentity identity) {
        this.identity = identity;
    }

    public void setLogGroupName(String logGroupName) {
        this.logGroupName = logGroupName;
    }

    public void setLogStreamName(String logStreamName) {
        this.logStreamName = logStreamName;
    }

    public void setLogger(LambdaLogger logger) {
        this.logger = logger;
    }

    public void setMemoryLimitInMB(int memoryLimitInMB) {
        this.memoryLimitInMB = memoryLimitInMB;
    }

    public void setRemainingTimeInMillis(int remainingTimeInMillis) {
        this.remainingTimeInMillis = remainingTimeInMillis;
    }

    public String getAwsRequestId() {
        return awsRequestId;
    }

    public ClientContext getClientContext() {
        return clientContext;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getFunctionVersion() {
        return null;
    }

    public String getInvokedFunctionArn() {
        return null;
    }

    public CognitoIdentity getIdentity() {
        return identity;
    }

    public String getLogGroupName() {
        return logGroupName;
    }

    public String getLogStreamName() {
        return logStreamName;
    }

    public LambdaLogger getLogger() {
        return logger;
    }

    public int getMemoryLimitInMB() {
        return memoryLimitInMB;
    }

    public int getRemainingTimeInMillis() {
        return remainingTimeInMillis;
    }

    private static class TestLogger implements LambdaLogger {


        public void log(String message) {
            System.err.println(message);
        }
    }
}
