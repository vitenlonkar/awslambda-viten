package com.graytrails.aws.lambdatest;

import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graytrails.aws.model.Movies;

import java.io.IOException;
import java.io.InputStream;

public class TestUtils {

    public static <T> T parse(String resource, Class<T> clazz, Object o)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        InputStream stream = TestUtils.class.getResourceAsStream(resource);
        try {
            if (clazz == S3Event.class) {
                String json = IOUtils.toString(stream);
                S3EventNotification event = S3EventNotification.parseJson(json);

                T result = (T) new S3Event(event.getRecords());
                return result;

            } else if (o == SQSEvent.class) {

                T result = mapper.readValue(stream, clazz);
                    return  result ;

            } else {
                return mapper.readValue(stream, clazz);
            }
        } finally {
            stream.close();
        }
    }



}
