package com.graytrails.aws.lambdatest;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graytrails.aws.lambda.LambdaFunctionHandlerToProcessSQSMessages;
import com.graytrails.aws.model.Movies;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LambdaFunctionHandlerToProcessSQSMessagesTest {

    private static SQSEvent input = new SQSEvent();

    @BeforeClass
    public static void createInput() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        SQSEvent.SQSMessage message= new SQSEvent.SQSMessage();
        message.setAwsRegion(Region.getRegion(Regions.EU_CENTRAL_1).toString());
        message.setBody(mapper.writeValueAsString(TestUtils.parse("/test.json", Movies.class, SQSEvent.class)));
        List<SQSEvent.SQSMessage> messageList = new ArrayList<SQSEvent.SQSMessage>();
        messageList.add(message);
        input.setRecords(messageList);

    }

    private Context createContext() {
        TestContext ctx = new TestContext();
        ctx.setFunctionName("handleRequest");
        return ctx;
    }

    @Test
    public void testLambdaFunctionHandler() {
        LambdaFunctionHandlerToProcessSQSMessages handler = new LambdaFunctionHandlerToProcessSQSMessages();
        Context ctx = createContext();
        handler.handleRequest(input, ctx);

    }
}
