package com.graytrails.aws.model;

public class Person {

    private String name ;
    private String lastName ;
    private Integer age ;
    public enum Sex {
        MALE, FEMALE
    }
    private Sex sex ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }


    @Override
    public String toString() {
        return this.lastName+"-"+this.name+"-"+this.age.toString()+"-"+this.sex;
    }
}
