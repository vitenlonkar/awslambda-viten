package com.graytrails.aws.samples;

import com.graytrails.aws.model.Person;

public interface LearnLamdaFunctionInterface {

     boolean test(Person p);

}
