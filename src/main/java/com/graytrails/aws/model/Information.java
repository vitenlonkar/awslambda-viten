package com.graytrails.aws.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.List;

@DynamoDBDocument
public class Information {
    private List<String> directors ;
    private String release_date;
    private List<String> genres ;
    private String rating;
    private String image_url ;
    private String plot ;
    private String rank ;
    private Integer running_time_secs ;
    private List<String> actors ;

    @DynamoDBAttribute(attributeName = "directors")
    public List<String> getDirectors() {
        return directors;
    }

    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }

   // @DynamoDBAttribute(attributeName = "release_date")
    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    //@DynamoDBAttribute(attributeName = "genres")
    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

   // @DynamoDBAttribute(attributeName = "rating")
    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

   // @DynamoDBAttribute(attributeName = "image_url")
    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

  //  @DynamoDBAttribute(attributeName = "plot")
    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

  //  @DynamoDBAttribute(attributeName = "rank")
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

  //  @DynamoDBAttribute(attributeName = "running_time_secs")
    public Integer getRunning_time_secs() {
        return running_time_secs;
    }

    public void setRunning_time_secs(Integer running_time_secs) {
        this.running_time_secs = running_time_secs;
    }

 //   @DynamoDBAttribute(attributeName = "actors")
    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }
}
