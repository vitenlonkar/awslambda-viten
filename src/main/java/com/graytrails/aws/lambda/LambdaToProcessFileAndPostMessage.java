package com.graytrails.aws.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graytrails.aws.model.Movies;

import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.List;

public class LambdaToProcessFileAndPostMessage implements RequestHandler<S3Event, String> {
    private AmazonS3 s3 = AmazonS3ClientBuilder.standard().build() ;
    private AmazonSQS sqs = AmazonSQSClientBuilder.standard().build() ;


    public LambdaToProcessFileAndPostMessage() {}
    public LambdaToProcessFileAndPostMessage(AmazonS3 s3, AmazonSQS sqs) {
        this.s3 = s3 ;
        this.sqs = sqs ;
    }

    public String handleRequest(S3Event input, Context context) {
        String returnResult = null ;

        context.getLogger().log("Received event: " + input);


        try {
            // Get bucketNAme
            String bucketName = input.getRecords().get(0).getS3().getBucket().getName();
            String key = input.getRecords().get(0).getS3().getObject().getKey();
            key = URLDecoder.decode(key, "UTF-8");
            context.getLogger().log("BucketName: " + bucketName);

            S3Object s3Object = s3.getObject(new GetObjectRequest(bucketName, key));
            InputStreamReader isr = new InputStreamReader(s3Object.getObjectContent());

            ObjectMapper mapper = new ObjectMapper();
            List<Movies> moviesList = mapper.readValue(isr, new TypeReference<List<Movies>>(){});

            context.getLogger().log("Started Parsing file from bucket: " + bucketName);

            //String queue_url = sqs.getQueueUrl("testSQSLambda").getQueueUrl();
            String queue_url = sqs.getQueueUrl(System.getenv("QUEUE_NAME")).getQueueUrl();
            context.getLogger().log("QUEUE_URL:" + queue_url);
            for(Movies movies : moviesList){
                context.getLogger().log(mapper.writeValueAsString(movies));
                SendMessageResult sendMessageResult = sqs.sendMessage(queue_url, mapper.writeValueAsString(movies));
                context.getLogger().log("Message Posted status MessageID: " + sendMessageResult.getMessageId()
                + "HttpMetaData: "+ sendMessageResult.getSdkHttpMetadata().getHttpStatusCode());

            }

            returnResult = "success" ;
        } catch (Exception e) {
            try {
                throw e ;
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }




        return returnResult ;
    }
}
